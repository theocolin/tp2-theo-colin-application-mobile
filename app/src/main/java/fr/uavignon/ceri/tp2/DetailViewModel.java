package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> selectedBook;


    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        selectedBook = repository.getSelectedBook();
    }

    public void insertOrUpdateBook(Book bookToInsertOrUpdate) {
        if(bookToInsertOrUpdate.getId() != 0) repository.updateBook(bookToInsertOrUpdate);
        else {
            repository.insertBook(bookToInsertOrUpdate);
        }
    }

    public Book getBook(long id) {
        return repository.getBook(id);
    }

    MutableLiveData<Book> getSelectedBook() { return selectedBook; }
}
