package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import java.util.Locale;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    public Book bookToUpdate;
    private DetailViewModel viewModel;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireParentFragment())
                .get(DetailViewModel.class);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        viewModel.getBook(args.getBookNum());
        observerSetup();

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = textTitle.getText().toString();
                String authors = textAuthors.getText().toString();
                String year = textYear.getText().toString();
                String genres = textGenres.getText().toString();
                String publisher = textPublisher.getText().toString();

                if(!title.equals("") && !authors.equals("")) {
                    if(bookToUpdate == null) {
                        bookToUpdate = new Book(title, authors, year, genres, publisher);
                    }
                    bookToUpdate.setTitle(title);
                    bookToUpdate.setAuthors(authors);
                    bookToUpdate.setYear(year);
                    bookToUpdate.setGenres(genres);
                    bookToUpdate.setPublisher(publisher);

                    viewModel.insertOrUpdateBook(bookToUpdate);
                    NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                            .navigate(R.id.action_SecondFragment_to_FirstFragment);
                }
                else {
                    Snackbar.make(view, "Attention titre ou auteurs manquants.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
    }

    private void observerSetup() {
        viewModel.getSelectedBook().observe(getViewLifecycleOwner(),
                new Observer<Book>() {
                    @Override
                    public void onChanged(Book book) {
                        bookToUpdate = book;

                        textTitle = (EditText) getView().findViewById(R.id.nameBook);
                        textAuthors = (EditText) getView().findViewById(R.id.editAuthors);
                        textYear = (EditText) getView().findViewById(R.id.editYear);
                        textGenres = (EditText) getView().findViewById(R.id.editGenres);
                        textPublisher = (EditText) getView().findViewById(R.id.editPublisher);

                        if(book != null) {
                            textTitle.setText(book.getTitle());
                            textAuthors.setText(book.getAuthors());
                            textYear.setText(book.getYear());
                            textGenres.setText(book.getGenres());
                            textPublisher.setText(book.getPublisher());
                        }
                    }
                });
    }
}